Red [
   description: {Red shell dialect}
   version: 0.1
   changelog: {
      0.1
      Parse commands, arguments, &&, || and pipes
      Run external commands, chain commands
   }
   todo: {
      * Add tests
   }
]

reform: func [block] [form reduce block]
reform-lines: func [block /local res] [
   res: copy {}
   forall block [
      append res to-string block/1
      append res newline
   ]
   res
]

shell-command: object [
   command: 'no-op
   external: no
   arguments: copy []
   redirects: copy []
   chain: none
   stdin:  none
   stdout: {}
   stderr: {}
   exit-code: none
   run: func [ /local cmd ] [
      exit-code: 0
      if external [
         forall arguments [
            if file? arguments/1 [ arguments/1: to-string arguments/1 ]
            if string? arguments/1 [
               ; replace arguments/1 {'} {\'}
               insert append arguments/1 {'} {'}
            ]
         ]
         cmd: reform [command arguments]
         exit-code: either stdin [
            if block? stdin [ stdin: reform-lines stdin ]
            call/wait/input/output/error cmd stdin stdout stderr
         ] [
            call/wait/output/error cmd stdout stderr
         ]
      ]
   ]
]

shell: context [
   current-command: none
   command-list: copy []
   built-in-commands: copy []

   rules: context [
      complete-command: [
         command any [c: command-separator (current-command/chain: c/1)  command]
      ]
      command-separator: [
         '&& | '|| | '| | 'out| | 'err|
      ]
      command: [
         [built-in | external] any argument any io-redirect
         (append/only command-list current-command)
      ]
      built-in: [
         c: built-ins 
         (cmd: select built-in-commands c/1
            current-command: make cmd [command: c/1])
      ]
      built-ins: ['no-op]
      external: [
         c: [word! | string!]
         (current-command: make shell-command [command: to-string c/1 external: yes])
      ]
      argument: [
         not [command-separator | io-redirect-command]
         c: skip (append current-command/arguments c/1)
      ]
      io-redirect: [
         c: io-redirect-command (append current-command/redirects c/1)
         c: io-target (append current-command/redirects c/1)
      ]
      io-redirect-command: [
            '<  |
            '>  |
            'out> |
            'err> |
            '>> |
            'out>> |
            'err>>
      ]
      io-target: [ file! | word! | string! ]
   ]

   just-parse: func [block] [
      parse block rules/complete-command
      command-list
   ]

   run: func [block /raw /debug /local cmd out-buffer err-buffer ret redirects inpt] [
      clear command-list
      either parse block rules/complete-command [
         forall command-list [
            cmd: command-list/1
            if inpt: find cmd/redirects '< [
               inpt: inpt/2
               if string? inpt [ inpt: to-file inpt ]
               cmd/stdin: case [
                  file? inpt [ read inpt ]
                  word? inpt [ get inpt ]
               ]
            ]
            if out-buffer [ cmd/stdin: out-buffer ]
            if error? ret: try [ cmd/run ] [
               cmd/exit-code: -666
               probe :ret
            ]
            if debug [
               print [ "debug>" cmd/command cmd/arguments cmd/redirects "|" cmd/exit-code ]
               if cmd/chain [ print [ "debug>" cmd/chain ] ]
            ]
            out-buffer: none
            switch cmd/chain [
               && [ if not zero? cmd/exit-code [ break ] ]
               || [ if zero? cmd/exit-code [ break ] ]
               |  [
                  out-buffer: cmd/stdout
                  if block? out-buffer [
                     ; transform Red's block into list of strings
                     out-buffer: reform-lines out-buffer
                  ]
               ]
            ]
            redirect-io cmd
         ]
         if cmd [
            if raw [ return cmd/stdout ]
            either string? cmd/stdout [
               split cmd/stdout newline
            ] [ cmd/stdout ]
         ]
      ] [
         ; add error-handling
         throw "can't parse your command"
      ]
   ]

   redirect-helper: func [target data /append] [
      if string? target [ target: to-file target ]
      case [
         word? target [
            either append [
               either series? target [ append get target data ] [set target data ]
            ] [ set target data ]
         ]
         file? target [
            either append [ write/append target data ] [ write target data ]
         ]
      ]
   ]

   redirect-io: func [cmd /local redirects rule target redirector] [
      redirects: cmd/redirects
      foreach [rule target] redirects [
         switch rule [
            > [ redirect-helper target cmd/stdout ]
            out> [ redirect-helper target cmd/stdout ]
            err> [ redirect-helper target cmd/stderr ]
            >> [ redirect-helper/append target cmd/stdout ]
            out>> [ redirect-helper/append target cmd/stdout ]
            err>> [ redirect-helper/append target cmd/stderr ]
         ]
      ]
   ]

   register: func [cmd] [
      if find words-of rules cmd/command [ throw reform ["name" cmd/command "is reserved" ] ]
      append rules/built-ins compose [| (to-lit-word cmd/command)]
      append built-in-commands cmd/command
      append built-in-commands cmd
   ]
]

; shell/register make shell-command [
;    ; dummy ls - just outputs content of first argument
;    command: 'ls
;    run: func [] [
;       either file? arguments/1 [
;          stdout: read arguments/1
;          exit-code: 0
;       ] [ exit-code: 127 ]
;    ]
; ]

; probe shell/run/debug [ls -a | grep "red" >> var && pwd > %file.txt]

; a: [4 2 5 7 1 6 7 1 9]
; probe shell/run/debug [sort < a]

; probe shell/run [true || echo "nothing"]
; probe shell/run [true && echo "test"]
; probe shell/run [false || echo "test"]
; probe shell/run [false && echo "nothing"]

; probe shell/run [cat %/etc/passwd | grep {root}]
; probe shell/run [ls %/ | grep {bin}]
; probe shell/run [ls %/ || echo "invalid argument"] ; lists /
; probe shell/run [ls 5 || echo "invalid argument"]  ; echoes error

; shell/run [pwd test err> errors out> output]

; print ["output:" output]
; print ["errors:" errors]

; probe shell/command-list
