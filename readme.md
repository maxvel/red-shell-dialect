# Red Shell Dialect

Shell dialect implementation for Red language.

## Features:

* Parsing shell-like commands
* Executing arbitrary external OS commands
* Extendable with Red code
* Redirecting IO to both files and Red variables
* Debug mode
* More coming! :)

## Examples:

    shell/run [true && echo "test"] ; => "test^/"
    shell/run [cat %/etc/passwd | grep {root}] ; => {root:x:0:0:root:/root:/bin/bash^/nm-openvpn:x:117:124:NetworkManager OpenVPN,,,:/var/lib/openvpn/chroot:/bin/false^/}

## Extend any way you like:

    shell/register make shell-command [
       ; dummy ls - just outputs content of first     argument
       command: 'ls
       run: func [] [
          either file? arguments/1 [
             stdout: read arguments/1
             exit-code: 0
          ] [ exit-code: 127 ]
       ]
    ]

    probe shell/run [ls %/ | grep {bin}] ; => "sbin/^/bin/^/"

## Redirects:

    shell/run [pwd test err> errors out> output]
    print ["output:" output]
    print ["errors:" errors]


Will output:

    output: /home/max/projects/red-shell-dialect

    errors: pwd: ignoring non-option arguments

Input:

    a: [4 2 5 7 1 6 7 1 9]
    probe shell/run [sort < a]

Outputs:

    ["1" "1" "2" "4" "5" "6" "7" "7" "9"]

## Debugging:

    shell/run/debug [ls / | sort -r | grep {doesnt-exist}]

Will output

    debug> ls /  | 0
    debug> |
    debug> sort -r  | 0
    debug> |
    debug> grep doesnt-exist  | 1
